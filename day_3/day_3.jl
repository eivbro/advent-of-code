using Test
using DelimitedFiles

test_map = readdlm("test-map.txt", String)
map = readdlm("map.txt", String)

function countTrees(map,right,down)
	counter = 0
	for i in 1:Int(floor(size(map)[1]/down))-1
		println("i = ", i)
		println(mod(right*i,length(map[1]))+1)
		if map[i*down+1][mod(right*i, length(map[1]))+1] == '#'
			counter +=1
		end
	end
	return counter
end

function findAnswerPartTwo(map, right, down)
	answer = 1
	for i in 1:length(right)
		answer *= countTrees(map, right[i], down[i])
	end
	return answer	
end

@test countTrees(test_map, 1, 1) == 2
@test countTrees(test_map, 3, 1) == 7
@test countTrees(test_map, 5, 1) == 3
@test countTrees(test_map, 7, 1) == 4
@test countTrees(test_map, 1, 2) == 2

right = (1, 3, 5, 7, 1)
down = (1, 1, 1, 1, 2)

findAnswerPartTwo(map, right, down)

