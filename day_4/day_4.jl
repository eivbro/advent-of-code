using Test
using DelimitedFiles

test_input = readdlm("test-input.txt", skipblanks=false)
test2_input = readdlm("test2-input.txt", skipblanks=false)
test3_input = readdlm("test3-input.txt", skipblanks=false)
input = readdlm("input.txt", skipblanks=false)

function countValidPassports(input)
	valid = 0
	cid = 0 
	counter = 0
	for i∈1:size(input)[1]
		if input[i,1] == ""
			if counter >= 7
				if counter == 8
					valid += 1
				elseif cid == 0
					valid += 1
				end
			end
			cid = 0
			counter = 0
		end
		for j∈1:size(input)[2]
			if input[i,j] != "" && checkFieldValid(input[i,j])
				counter += 1
				if input[i,j][1:3] == "cid"
					cid = 1
				end
			end
		end
	end
	return valid
end

function checkFieldValid(field)
	valid = false
	type = field[1:3]
	identifier = field[5:lastindex(field)]
	number_of_chars = length(identifier)
	ecl = ["amb" "blu" "brn" "gry" "grn" "hzl" "oth"]
	if type == "cid"
		valid = true
	elseif type == "byr"
		if number_of_chars == 4 && 1920 <= parse(Int, identifier) <= 2003
			valid = true
		end
	elseif type == "iyr"
		if number_of_chars == 4 && 2010 <= parse(Int, identifier) <= 2020
			valid = true
		end
	elseif type == "eyr"
		if number_of_chars == 4 && 2020 <= parse(Int, identifier) <= 2030
			valid = true
		end
	elseif type == "hgt"
		if identifier[number_of_chars - 1: number_of_chars] == "cm"
			if 150 <= parse(Int, identifier[1:number_of_chars - 2]) <= 193
				valid = true
			end
		elseif identifier[number_of_chars - 1: number_of_chars] == "in"
			if 59 <= parse(Int, identifier[1:number_of_chars - 2]) <= 76 
				valid = true
			end
		end
	elseif type == "ecl"
		if number_of_chars == 3
			for i∈ecl
				if i == identifier
					valid = true
					break
				end
			end
		end
	elseif type == "hcl"
		if number_of_chars == 7 && identifier[1] == '#'
			valid = true
			for i∈identifier[2:number_of_chars]
				if !(isdigit(i))
					if match(r"[a-f]", string(i)) == nothing
						valid = false
					end
				end
			end
		end
	elseif number_of_chars == 9
		valid = true
		for i∈identifier
			if !(isdigit(i))
				valid = false
			end
		end
	end
	return valid
end

#@test countValidPassports(test_input) == 2
#@test countValidPassports(test3_input) == 4
countValidPassports(input)
