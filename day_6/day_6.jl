using Test
using DelimitedFiles

test_input = readdlm("test-input.txt", skipblanks=false)
input = readdlm("input.txt", skipblanks=false)

function checkGroup(input)
	q = Char.('a'[1]:'z'[1])
	x = zeros(27) 
	for e in input
		for i in 1:lastindex(q)
			if q[i] == e
				x[i] = 1
			end
		end
	end
	return Int(sum(x))
end

function checkGroup(input, members)
	q = Char.('a'[1]:'z'[1])
	x = Int.(zeros(27)) 
	yeses = 0
	for e in input
		for i in 1:lastindex(q)
			if q[i] == e
				x[i] += 1
				if x[i] == members
					yeses += 1
				end
			end
		end
	end
	return yeses
end


function countYesesPartOne(input)
	group = ""
	yeses = 0
	for e in input
		if e != ""
			group = string(group, e)
		else
			yeses += checkGroup(group)
			group = ""
		end
	end
	return yeses
end

function countYesesPartTwo(input)
	group = ""
	yeses = 0
	members = 0
	for e in input
		if e != ""
			members += 1
			group = string(group, e)
		else
			yeses += checkGroup(group, members)
			group = ""
			members = 0
		end
	end
	return yeses
end

@test countYesesPartOne(test_input) == 11
@test countYesesPartTwo(test_input) == 6

println("Answer part one = ", countYesesPartOne(input))
println("Answer part two = ", countYesesPartTwo(input))
