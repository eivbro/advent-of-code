using Test
using DelimitedFiles

#Importing test puzzle and my puzzle
test_input = readdlm("test-input.txt", String)
puzzle_input = readdlm("input-day2.txt", String)
	
#Function for finding the the position of the hyphen
function findSeparatorIndex(range)
	for i in 1:length(range)
		if range[i] == '-'
			return i
		end
	end
end

#Converting the range for allowed numbers from a string to two integers
function findMinMax(range)
	separator_index = findSeparatorIndex(range)
	min = range[1:separator_index-1]
	max = range[separator_index+1:length(range)]
	return [parse(Int, min) parse(Int, max)]
end

#Counting the number of occurences of the target letter in a password
function countInstances(puzzle_instance)
	password = puzzle_instance[3]
	target_letter = puzzle_instance[2][1]
	counter = 0
	for letter in password
		if letter == target_letter
			counter += 1
		end
	end
	return counter
end

#Checking all the passwords for part one
function checkPasswords(puzzle_input)
	counter = 0
	for i in 1:length(puzzle_input[:,1])	
		min_max = findMinMax(puzzle_input[i,1])
		instances = countInstances(puzzle_input[i,:])
		if min_max[1] <= instances <= min_max[2]
			counter += 1
		end
	end
	return counter
end

#Checking all the passwords for part two
function checkPasswordsPartTwo(puzzle_input)
	counter = 0
	for i in 1:length(puzzle_input[:,1])
		letter = puzzle_input[i,2][1]
		min_max = findMinMax(puzzle_input[i,1])
		if xor(letter == puzzle_input[i,3][min_max[1]], letter == puzzle_input[i,3][min_max[2]])
			counter += 1
		end
	end
	return counter
end

#Running tests
@test checkPasswords(test_input) == 2
@test checkPasswordsPartTwo(test_input) == 1
println("Answer part 1: ", checkPasswords(puzzle_input))
println("Answer part 2: ", checkPasswordsPartTwo(puzzle_input))
		
