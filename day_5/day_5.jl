using Test

input = readlines("input.txt")

function y(input)
	result = 0:127
	for i in 1:7
		if input[i] == 'F'
			result = result[1:2^(7-i)]
		elseif input[i] == 'B'
			result = result[Int(ceil(lastindex(result)/2))+1:lastindex(result)]
		end
	end
	return result[1]
end

function x(input)
	result = 0:7
	for i in 1:3
		if input[i+7] == 'L'
			result = result[1:2^(3-i)]
		elseif input[i+7] == 'R'
			result = result[Int(ceil(lastindex(result)/2))+1:lastindex(result)]
		end
	end
	return result[1]
end

function seatId(input)
	return y(input)*8 + x(input)
end

function findMax(input)
	max = 0
	for e in input
		if seatId(e) > max
			max = seatId(e)
		end
	end
	return max
end

function findSeatId(input)
	ids = collect(1:findMax(input))
	start = 0
	for e in input
		ids[seatId(e)] = 0
	end
	for i in 1:lastindex(ids)
		if ids[i] == 0
			start = i
			break
		end
	end
	for e in ids[start+1:lastindex(ids)]
		if e != 0
			return e
		end
	end
end


@test y("FBFBBFFRLR") == 44
@test y("BFFFBBFRRR") == 70
@test y("FFFBBBFRRR") == 14
@test y("FFFBBBFRRR") == 14

@test x("FBFBBFFRLR") == 5
@test x("BFFFBBFRRR") == 7
@test x("FFFBBBFRRR") == 7
@test x("BBFFBBFRLL") == 4

println("Max seat id = ", findMax(input))
println("My seat id = ", findSeatId(input))


