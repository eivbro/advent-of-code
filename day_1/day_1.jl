using Test
using DelimitedFiles

puzzle_input = readdlm("input1_1.txt", Int64)

function calculateExpenseTwo(expenses)
	size = length(expenses)
	for i in 1:size
		for j in 1:size
			if expenses[i]+expenses[j] == 2020
				answer = expenses[i]*expenses[j]
				return(answer)
			end
		end
	end
	println("No response")
end

function calculateExpenseThree(expenses)
	size = length(expenses)
	for i in 1:size
		for j in 1:size
			for k in 1:size
				sum = expenses[i]+expenses[j]+expenses[k]
				if sum == 2020
					answer = expenses[i]*expenses[j]*expenses[k]
					return(answer)
				end
			end
		end
	end
	println("No response")
end

expenses_test = collect((1721, 979, 366, 299, 675, 1456))
@test calculateExpenseTwo(expenses_test) == 514579
@test calculateExpenseThree(expenses_test) == 241861950

answer1 = calculateExpenseTwo(puzzle_input)
answer2 = calculateExpenseThree(puzzle_input)
println("Answer part 1: ", answer1)
println("Answer part 2: ", answer2)
